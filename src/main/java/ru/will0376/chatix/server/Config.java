package ru.will0376.chatix.server;

import net.minecraftforge.common.config.Configuration;
import ru.justagod.cutter.GradleSide;
import ru.justagod.cutter.GradleSideOnly;

import java.io.File;

@GradleSideOnly(GradleSide.SERVER)
public class Config {
	private final Configuration configuration;
	private boolean disablePerms;
	private int limit;
	private boolean disableHover;

	public Config(File file) {
		this.configuration = new Configuration(file);
	}

	public void launch() {
		this.load();
		this.setConfigs();
		this.save();
	}

	private void load() {
		this.configuration.load();
	}

	private void setConfigs() {
		disablePerms = this.configuration.getBoolean("disablePerms", "general", false, "");
		disableHover = this.configuration.getBoolean("disableHover", "general", false, "");
		limit = this.configuration.getInt("limit", "general", 1, Integer.MIN_VALUE, Integer.MAX_VALUE, "");
	}

	private void save() {
		this.configuration.save();
	}

	public boolean isDisablePerms() {
		return disablePerms;
	}

	public boolean isDisableHover() {
		return disableHover;
	}

	public int getLimit() {
		return limit;
	}
}
