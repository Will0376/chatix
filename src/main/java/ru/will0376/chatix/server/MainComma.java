package ru.will0376.chatix.server;

import net.minecraft.command.CommandBase;
import net.minecraft.command.CommandException;
import net.minecraft.command.ICommandSender;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.server.MinecraftServer;
import net.minecraftforge.fml.common.FMLCommonHandler;
import ru.justagod.cutter.GradleSide;
import ru.justagod.cutter.GradleSideOnly;
import ru.will0376.chatix.Chatix;
import ru.will0376.chatix.common.net.ToClient;

import java.io.IOException;

@GradleSideOnly(GradleSide.SERVER)
public class MainComma extends CommandBase {
	@Override
	public String getName() {
		return "chatix";
	}

	@Override
	public String getUsage(ICommandSender sender) {
		return "/chatix reload";
	}

	@Override
	public int getRequiredPermissionLevel() {
		return 4;
	}

	@Override
	public void execute(MinecraftServer server, ICommandSender sender, String[] args) throws CommandException {
		if (args.length >= 1) {
			if (args[0].equals("reload")) {
				Chatix.config.launch();
				server.getPlayerList().getPlayers().forEach(e -> {
					if (Chatix.server) {
						SmileConf.jsons.clear();
						try {
							new SmileConf().load();
						} catch (IOException ioException) {
							ioException.printStackTrace();
						}
						if (FMLCommonHandler.instance().getSide().isServer()) {
							Chatix.network.sendTo(new ToClient(2, "", Chatix.config.isDisableHover(), Chatix.config.getLimit()), e);
							SmileConf.jsons.forEach(j -> Chatix.network.sendTo(new ToClient(3, j.toString(), Chatix.config.isDisableHover(), Chatix.config.getLimit()), (EntityPlayerMP) sender));
						}
					}
				});
			}
		}
	}
}
