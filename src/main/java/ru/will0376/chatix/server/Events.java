package ru.will0376.chatix.server;

import com.google.gson.JsonObject;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.util.text.Style;
import net.minecraft.util.text.TextComponentTranslation;
import net.minecraft.util.text.TextFormatting;
import net.minecraftforge.event.ServerChatEvent;
import net.minecraftforge.event.world.WorldEvent;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;
import net.minecraftforge.fml.common.gameevent.PlayerEvent;
import ru.justagod.cutter.GradleSide;
import ru.justagod.cutter.GradleSideOnly;
import ru.will0376.chatix.Chatix;
import ru.will0376.chatix.common.PlayerStatistic;
import ru.will0376.chatix.common.net.ToClient;
import ru.will0376.chatix.utils.Parse;

import java.util.Optional;

@GradleSideOnly(GradleSide.SERVER)
@Mod.EventBusSubscriber
public class Events {

	@SubscribeEvent
	public static void joinPlayer(PlayerEvent.PlayerLoggedInEvent event) {
		try {
			if (Chatix.server) {
				Chatix.network.sendTo(new ToClient(2, "", Chatix.config.isDisableHover(), Chatix.config.getLimit()), (EntityPlayerMP) event.player);
				Chatix.network.sendTo(new ToClient(-1,
						PlayerStatistic.safeGetProfile(event.player.getName()).getJson().toString(),
						Chatix.config.isDisableHover(),
						Chatix.config.getLimit()), (EntityPlayerMP) event.player);

				SmileConf.jsons.forEach(j -> Chatix.network.sendTo(new ToClient(3, j.toString(), Chatix.config.isDisableHover(), Chatix.config.getLimit()), (EntityPlayerMP) event.player));
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@SubscribeEvent
	public static void chatChecker(ServerChatEvent event) {
		String message = event.getMessage();
		String validInput = message.replaceAll("(§.)", "");
		int limit = Chatix.config.getLimit();
		int i = 0;
		for (String usage : Parse.parseSmilesUsages(validInput)) {
			for (; message.contains(usage); message = message.replaceFirst(usage, "")) {
				Optional<JsonObject> jsonObject = SmileConf.fromString(usage);
				jsonObject.ifPresent((json) -> {
					if (!event.getPlayer().canUseCommand(4, json.get("permission").getAsString()) && !Chatix.config.isDisablePerms()) {
						event.setCanceled(true);
						event.getPlayer().sendMessage(new TextComponentTranslation("chatix.server.checkmessage.permission.error")
								.setStyle(new Style().setColor(TextFormatting.RED)));
					}
				});

				if (event.isCanceled()) return;

				i++;
				if (i > limit) {
					event.setCanceled(true);
					event.getPlayer().sendMessage(new TextComponentTranslation("chatix.server.checkmessage.emoji.limit", limit)
							.setStyle(new Style().setColor(TextFormatting.RED)));
					return;
				}
				jsonObject.ifPresent((json) -> {
					PlayerStatistic.serverIncrement(event.getPlayer().getName(), json.get("name").getAsString());
					Chatix.network.sendTo(new ToClient(-2, json.get("name").getAsString(), Chatix.config.isDisableHover(), Chatix.config.getLimit()), event.getPlayer());
				});
			}
		}
	}

	@SubscribeEvent
	public static void onWorldSave(WorldEvent.Save event) {
		PlayerStatistic.safeSaveAll();
	}
}
