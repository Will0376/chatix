package ru.will0376.chatix.server;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import ru.justagod.cutter.GradleSide;
import ru.justagod.cutter.GradleSideOnly;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Optional;

@GradleSideOnly(GradleSide.SERVER)
public class SmileConf {
	public static ArrayList<JsonObject> jsons = new ArrayList<>();
	public final File rootFolder;

	public SmileConf() {
		this.rootFolder = new File(".", "ChatiXCommon");
	}

	public static Optional<JsonObject> fromString(String string) {
		for (JsonObject jsonObject : jsons) {
			for (JsonElement jsonElement : jsonObject.getAsJsonArray("usage")) {
				if (string.contains(jsonElement.getAsString()))
					return Optional.of(jsonObject);
			}
		}
		return Optional.empty();
	}

	public void load() throws IOException {
		if (rootFolder.exists() || rootFolder.mkdir()) {
			Files.walk(Paths.get(rootFolder.getAbsolutePath()))
					.filter(Files::isRegularFile)
					.forEach(e -> {
						try {
							if (e != null && e.toString().contains(".json")) {
								jsons.add((JsonObject) new JsonParser().parse(new FileReader(e.toString())));
							}
						} catch (Exception ex) {
							ex.printStackTrace();
						}
					});
		}
	}
}
