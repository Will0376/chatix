package ru.will0376.chatix.server;

import net.minecraft.command.CommandBase;
import net.minecraft.command.CommandException;
import net.minecraft.command.ICommandSender;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.server.MinecraftServer;
import ru.justagod.cutter.GradleSide;
import ru.justagod.cutter.GradleSideOnly;
import ru.will0376.chatix.Chatix;
import ru.will0376.chatix.common.net.ToClient;

@GradleSideOnly(GradleSide.SERVER)
public class CommandGen extends CommandBase {
	@Override
	public String getName() {
		return "chatixGenerate";
	}

	@Override
	public String getUsage(ICommandSender sender) {
		return "/chatixGenerate";
	}

	@Override
	public int getRequiredPermissionLevel() {
		return 4;
	}

	@Override
	public void execute(MinecraftServer server, ICommandSender sender, String[] args) throws CommandException {
		if (!sender.getName().equals("server")) if (sender.canUseCommand(4, "chatix.generate.configs")) {
			Chatix.network.sendTo(new ToClient(4, "", Chatix.config.isDisableHover(), Chatix.config.getLimit()), (EntityPlayerMP) sender);//Рескан
			Chatix.network.sendTo(new ToClient(1, "", Chatix.config.isDisableHover(), Chatix.config.getLimit()), (EntityPlayerMP) sender);//Создание файлов
		}
	}
}
