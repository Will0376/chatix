package ru.will0376.chatix;

import net.minecraft.launchwrapper.Launch;
import net.minecraftforge.fml.common.FMLCommonHandler;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.SidedProxy;
import net.minecraftforge.fml.common.event.FMLInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPostInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPreInitializationEvent;
import net.minecraftforge.fml.common.event.FMLServerStartingEvent;
import net.minecraftforge.fml.common.network.NetworkRegistry;
import net.minecraftforge.fml.common.network.simpleimpl.SimpleNetworkWrapper;
import net.minecraftforge.fml.relauncher.Side;
import ru.justagod.cutter.GradleSide;
import ru.justagod.cutter.GradleSideOnly;
import ru.justagod.cutter.invoke.Invoke;
import ru.will0376.chatix.common.PlayerStatistic;
import ru.will0376.chatix.common.net.ToClient;
import ru.will0376.chatix.server.CommandGen;
import ru.will0376.chatix.server.Config;
import ru.will0376.chatix.server.MainComma;
import ru.will0376.chatix.server.SmileConf;

import java.io.File;
import java.io.IOException;

@Mod(modid = Chatix.MOD_ID, name = Chatix.MOD_NAME, version = Chatix.VERSION)
public class Chatix {

	public static final String MOD_ID = "chatix";
	public static final String MOD_NAME = "Chatix";
	public static final String VERSION = "1.4";
	public static boolean debug = true, server = false;
	public static File configFolder;
	@SidedProxy(clientSide = "ru.will0376.chatix.client.ClientProxy", serverSide = "ru.will0376.chatix.CommonProxy")
	public static CommonProxy proxy;
	public static SimpleNetworkWrapper network;
	@GradleSideOnly(GradleSide.SERVER)
	public static Config config;
	@Mod.Instance(MOD_ID)
	public static Chatix INSTANCE;

	@Mod.EventHandler
	public void preinit(FMLPreInitializationEvent event) {
		if (debug) debug = (boolean) Launch.blackboard.get("fml.deobfuscatedEnvironment");
		server = FMLCommonHandler.instance().getSide().isServer() || debug;
		proxy.preInit(event);
		configFolder = event.getModConfigurationDirectory();
		Invoke.server(() -> {
			config = new Config(event.getSuggestedConfigurationFile());
			PlayerStatistic.setPlayersDir(event.getModConfigurationDirectory());
			config.launch();
		});

	}

	@Mod.EventHandler
	public void init(FMLInitializationEvent event) {
		proxy.init(event);
		network = NetworkRegistry.INSTANCE.newSimpleChannel(MOD_ID);
		network.registerMessage(new ToClient.Handler(), ToClient.class, 1, Side.CLIENT);
	}

	@Mod.EventHandler
	public void postinit(FMLPostInitializationEvent event) {
		proxy.postInit(event);
	}

	@GradleSideOnly(GradleSide.SERVER)
	@Mod.EventHandler
	public void serverInit(FMLServerStartingEvent event) {
		try {
			new SmileConf().load();
			PlayerStatistic.loadPlayersStatistics();
		} catch (IOException e) {
			e.printStackTrace();
			FMLCommonHandler.instance().handleExit(0);
		}
		event.registerServerCommand(new CommandGen());
		event.registerServerCommand(new MainComma());
	}
}