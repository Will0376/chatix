package ru.will0376.chatix.utils;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

import java.util.ArrayList;
import java.util.Map;
import java.util.Set;

public class JsonUtils {
	JsonObject jo;

	public JsonUtils() {
		jo = new JsonObject();
	}

	public static Set<Map.Entry<String, JsonElement>> jsonToEntry(JsonObject jo) {
		return jo.entrySet();
	}

	public static ArrayList<String> convertJA(JsonArray ja) {
		ArrayList<String> tmp = new ArrayList<>();
		ja.forEach(e -> tmp.add(e.getAsString()));
		return tmp;
	}

	public JsonUtils addString(String name, String value) {
		jo.addProperty(name, value);
		return this;
	}

	public JsonUtils setJson(JsonObject jo) {
		this.jo = jo;
		return this;
	}

	public JsonUtils addArrayString(String name, ArrayList<String> array) {
		JsonArray ja = new JsonArray();
		array.forEach(ja::add);
		jo.add(name, ja);
		return this;
	}

	public JsonUtils addHashMap(String name, Map<String, Long> map) {
		JsonObject jo = new JsonObject();
		map.forEach(jo::addProperty);
		this.jo.add(name, jo);
		return this;
	}

	public JsonUtils addNum(String name, Number value) {
		jo.addProperty(name, value);
		return this;
	}

	public JsonObject build() {
		return jo;
	}
}
