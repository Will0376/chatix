package ru.will0376.chatix.utils;

import net.minecraftforge.fml.common.FMLCommonHandler;
import ru.justagod.cutter.invoke.Invoke;
import ru.will0376.chatix.client.utils.PojoSmile;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class Parse {

	public static List<String> parseSmilesUsages(String text) {
		List<String> usages = new ArrayList<>();

		if (FMLCommonHandler.instance().getSide().isClient()) {
			Invoke.client(() -> {
				Map<String, PojoSmile> pojoUsages = PojoSmile.getUsages();

				for (String token : text.split(" ")) {
					if (pojoUsages.containsKey(token)) {
						usages.add(token);
					}
				}
			});
		} else {
			Invoke.server(() ->
					ru.will0376.chatix.server.SmileConf.jsons.forEach(json ->
							json.get("usage").getAsJsonArray().forEach(element -> {
								if (text.contains(element.getAsString())) usages.add(element.getAsString());
							})));

		}
		//Collections.reverse(usages);
		return usages;

	}
}
