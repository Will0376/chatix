package ru.will0376.chatix.client.utils;

import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.GuiButton;
import net.minecraft.client.renderer.BufferBuilder;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.client.renderer.Tessellator;
import net.minecraft.client.renderer.vertex.DefaultVertexFormats;
import net.minecraft.util.ResourceLocation;
import ru.justagod.cutter.GradleSide;
import ru.justagod.cutter.GradleSideOnly;
import ru.will0376.chatix.Chatix;

@GradleSideOnly(GradleSide.CLIENT)
public class GuiButtonSome extends GuiButton {
	String pathToImg;
	String pathToSelectedImg;
	ResourceLocation selectedResource = null;
	int h, w;
	String text;
	float scalledX = 1, scalledY = 1, zLevel = 2;
	boolean drawText;

	public GuiButtonSome(int buttonId, String path, String selectedPath, int x, int y, int widthIn, int heightIn, float scalledX, float scalledY, float zLevel, String buttonText) {
		this(buttonId, path, selectedPath, x, y, widthIn, heightIn, buttonText, true);
		this.scalledX = scalledX;
		this.scalledY = scalledY;
		this.zLevel = zLevel;
	}

	public GuiButtonSome(int buttonId, String path, String selectedPath, int x, int y, int widthIn, int heightIn, String buttonText) {
		this(buttonId, path, selectedPath, x, y, widthIn, heightIn, buttonText, false);
	}

	public GuiButtonSome(int buttonId, String path, String selectedPath, int x, int y, int widthIn, int heightIn) {
		this(buttonId, path, selectedPath, x, y, widthIn, heightIn, "", false);
	}

	public GuiButtonSome(int buttonId, String path, String selectedPath, int x, int y, int widthIn, int heightIn, String buttonText, boolean drawText) {
		super(buttonId, x, y, widthIn, heightIn, buttonText);
		h = heightIn;
		w = widthIn;
		pathToImg = Chatix.MOD_ID + ":" + path;
		pathToSelectedImg = Chatix.MOD_ID + ":" + selectedPath;
		text = buttonText;
		this.drawText = drawText;
	}

	@Override
	public void drawButton(Minecraft mc, int mouseX, int mouseY, float partialTicks) {
		if (this.visible) {
			mc.getTextureManager().bindTexture(new ResourceLocation(pathToImg));
			GlStateManager.color(1.0F, 1.0F, 1.0F, 1.0F);
			this.hovered = mouseX >= this.x && mouseY >= this.y && mouseX < this.x + this.width && mouseY < this.y + this.height;
			if (this.hovered)
				if (!pathToSelectedImg.contains("null"))
					mc.getTextureManager().bindTexture(new ResourceLocation(pathToSelectedImg));
				else if (selectedResource != null) mc.getTextureManager().bindTexture(selectedResource);
			GlStateManager.enableBlend();
			GlStateManager.tryBlendFuncSeparate(GlStateManager.SourceFactor.SRC_ALPHA, GlStateManager.DestFactor.ONE_MINUS_SRC_ALPHA, GlStateManager.SourceFactor.ONE, GlStateManager.DestFactor.ZERO);
			GlStateManager.blendFunc(GlStateManager.SourceFactor.SRC_ALPHA, GlStateManager.DestFactor.ONE_MINUS_SRC_ALPHA);
			if (!hovered) {
				h = 12;
				w = 12;
			}
			draw();
			this.mouseDragged(mc, mouseX, mouseY);
			if (drawText)
				GuiHelper.drawScalledCenteredString(mc.fontRenderer, this.x + this.width / 2, this.y + (this.height - 8) / 2, scalledX, scalledY, zLevel + 1, text, 0xFFFFFF);
		}
	}

	public String getPathToImg() {
		return pathToImg;
	}

	public void setPathToImg(String pathToImg) {
		this.pathToImg = pathToImg;
	}

	public String getPathToSelectedImg() {
		return pathToSelectedImg;
	}

	public void setPathToSelectedImg(String pathToSelectedImg) {
		this.pathToSelectedImg = pathToSelectedImg;
	}

	public ResourceLocation getSelectedResource() {
		return selectedResource;
	}

	public void setSelectedResource(ResourceLocation selectedResource) {
		this.selectedResource = selectedResource;
	}

	public int getH() {
		return h;
	}

	public void setH(int h) {
		this.h = h;
	}

	public int getW() {
		return w;
	}

	public void setW(int w) {
		this.w = w;
	}

	public void addX(int x) {
		this.x += x;
	}

	public void addY(int y) {
		this.y += y;
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	public float getScalledX() {
		return scalledX;
	}

	public void setScalledX(float scalledX) {
		this.scalledX = scalledX;
	}

	public float getScalledY() {
		return scalledY;
	}

	public void setScalledY(float scalledY) {
		this.scalledY = scalledY;
	}

	public float getzLevel() {
		return zLevel;
	}

	public void setzLevel(float zLevel) {
		this.zLevel = zLevel;
	}

	public boolean isDrawText() {
		return drawText;
	}

	public void setDrawText(boolean drawText) {
		this.drawText = drawText;
	}

	private void draw() {
		Tessellator tessellator = Tessellator.getInstance();
		BufferBuilder bufferbuilder = tessellator.getBuffer();
		bufferbuilder.begin(7, DefaultVertexFormats.POSITION_TEX);
		bufferbuilder.pos(x, y + h, zLevel).tex(0, 1).endVertex();
		bufferbuilder.pos(x + w, y + h, zLevel).tex(1, 1).endVertex();
		bufferbuilder.pos(x + w, y, zLevel).tex(1, 0).endVertex();
		bufferbuilder.pos(x, y, zLevel).tex(0, 0).endVertex();
		tessellator.draw();
	}
}
