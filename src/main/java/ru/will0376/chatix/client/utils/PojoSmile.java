package ru.will0376.chatix.client.utils;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import net.minecraft.util.ResourceLocation;
import ru.justagod.cutter.GradleSide;
import ru.justagod.cutter.GradleSideOnly;
import ru.will0376.chatix.utils.JsonUtils;

import javax.imageio.ImageIO;
import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

@GradleSideOnly(GradleSide.CLIENT)
public class PojoSmile {
	private static final Map<String, PojoSmile> usages = new HashMap<>();
	private static final ArrayList<PojoSmile> list = new ArrayList<>();
	private final String name;
	private final String path;
	private final int id;
	private final String permission;
	private final ArrayList<String> usage;
	volatile ResourceLocation resourceLocation;
	private int offsetX = 0;
	private int offsetY = 0;

	public PojoSmile(String name, String path, int id, String permission, ArrayList<String> usage, int offsetX, int offsetY) { // Специально для быстрого создания json'а
		this.name = name;
		this.path = path;
		this.id = id;
		this.permission = permission;
		this.usage = usage;
		this.offsetX = offsetX;
		this.offsetY = offsetY;
		net.minecraft.client.Minecraft.getMinecraft().addScheduledTask(() -> {
			try {
				this.resourceLocation = loadResource(name, new File(".", path));
			} catch (Exception e) {
				System.err.println("Error: " + name);
				System.err.println("Path: " + path);
				e.printStackTrace();
			}
		});
		list.add(this);
		usage.forEach(aliases -> usages.put(aliases, this));
	}

	public static void clearAll() {
		usages.clear();
		list.clear();
	}

	public static Map<String, PojoSmile> getUsages() {
		return usages;
	}

	public static ArrayList<PojoSmile> getList() {
		return list;
	}

	public static PojoSmile createFromJson(JsonObject jo) {
		return new PojoSmile(jo.get("name").getAsString(),
				jo.get("path").getAsString(),
				jo.get("id").getAsInt(),
				jo.get("permission").getAsString(),
				JsonUtils.convertJA(jo.getAsJsonArray("usage")),
				jo.has("offsetX") ? jo.get("offsetX").getAsInt() : 0,
				jo.has("addy") ? jo.get("addy").getAsInt() : 0
		);
	}

	public static PojoSmile getRandom() {
		return list.get((int) (Math.random() * list.size()));
	}

	@GradleSideOnly(GradleSide.CLIENT)
	public ResourceLocation getResourceLocation() {
		return resourceLocation;
	}

	public String getName() {
		return name;
	}

	public String getPath() {
		return path;
	}

	public int getId() {
		return id;
	}

	public String getPermission() {
		return permission;
	}

	public ArrayList<String> getUsage() {
		return usage;
	}

	public int getWidth() {
		return 12 + offsetX;
	}

	public int getOffsetX() {
		return offsetX;
	}

	public int getOffsetY() {
		return offsetY;
	}

	public int getHeight() {
		return 12 + offsetY;
	}

	public ResourceLocation loadResource(String name, File path) throws Exception {
		return net.minecraft.client.Minecraft.getMinecraft().getTextureManager().getDynamicTextureLocation(name, new net.minecraft.client.renderer.texture.DynamicTexture(ImageIO.read(path)));
	}

	public JsonObject getJson() {
		Gson gson = new GsonBuilder().setPrettyPrinting().create();
		return (JsonObject) new JsonParser().parse(gson.toJson(this));
	}
}
