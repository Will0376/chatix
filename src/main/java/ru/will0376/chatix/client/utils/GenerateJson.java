package ru.will0376.chatix.client.utils;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;
import net.minecraft.client.Minecraft;
import ru.justagod.cutter.GradleSide;
import ru.justagod.cutter.GradleSideOnly;
import ru.will0376.chatix.utils.JsonUtils;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.net.URI;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;

@GradleSideOnly(GradleSide.CLIENT)
public class GenerateJson {
	public static ArrayList<File> smileFiles = new ArrayList<>();
	public static File smileFolder = new File(Minecraft.getMinecraft().gameDir, "ChatiX");

	public GenerateJson(File file) {
		URI relativize = Minecraft.getMinecraft().gameDir.toURI().relativize(new File(file.getAbsolutePath()).toURI());
		String path = relativize.getPath();
		StringBuilder stringBuilder = new StringBuilder(path);
		//stringBuilder.setCharAt(path.lastIndexOf("/"), '_');
		String[] args = stringBuilder.toString().split("/");
		String name = args[args.length - 1].replaceAll(".png", "");
		int id = smileFiles.indexOf(file);
		ArrayList<String> list = new ArrayList<>(); //Оставь 2 пункта, название и ид
		list.add(String.format(":%s:", args[args.length - 2] + "_" + name));
		list.add(String.format(":%s:", id));
		safeSaveFile(new JsonUtils().addString("path", path)
				.addString("name", name)
				.addNum("id", id)
				.addString("permission", "chatix." + args[args.length - 2] + "." + name)
				.addArrayString("usage", list)
				.build());
	}

	public static void generateFiles() {
		GenerateJson.smileFiles.forEach(GenerateJson::new);
	}

	public static void deepScanFiles() {
		if (smileFolder.exists() || smileFolder.mkdir()) {
			try {
				Files.walk(Paths.get(smileFolder.getAbsolutePath()))
						.filter(Files::isRegularFile)
						.forEach(e -> {
							if (!e.toString().contains("skipme")) smileFiles.add(new File(e.toString()));
						});
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

	private void safeSaveFile(JsonObject jo) {
		File root = new File(".", "ChatiXCommon");
		if (root.exists() || root.mkdir()) {
			String path = jo.get("path").getAsString().replace("ChatiX/", "");
			File pathToFile = new File(root, path.substring(0, path.lastIndexOf("/")));
			if (pathToFile.exists() || pathToFile.mkdirs()) {
				try (FileWriter writer = new FileWriter(new File(pathToFile, jo.get("name").getAsString() + ".json"))) {
					Gson gson = new GsonBuilder().setPrettyPrinting().create();
					gson.toJson(jo, writer);
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
	}
}
