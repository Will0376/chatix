package ru.will0376.chatix.client;

import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.GuiChat;
import net.minecraftforge.client.event.GuiOpenEvent;
import net.minecraftforge.client.event.RenderGameOverlayEvent;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;
import net.minecraftforge.fml.common.gameevent.PlayerEvent;
import net.minecraftforge.fml.common.gameevent.TickEvent;
import net.minecraftforge.fml.relauncher.Side;
import ru.justagod.cutter.GradleSide;
import ru.justagod.cutter.GradleSideOnly;
import ru.will0376.chatix.client.utils.PojoSmile;

@GradleSideOnly(GradleSide.CLIENT)
@Mod.EventBusSubscriber(Side.CLIENT)
public class EventsClient {

	@SubscribeEvent
	public static void openTab(RenderGameOverlayEvent.Chat e) {
		Minecraft mc = Minecraft.getMinecraft();
		if (!(mc.ingameGUI.persistantChatGUI instanceof ChatGui)) mc.ingameGUI.persistantChatGUI = new ChatGui(mc);
	}

	@SubscribeEvent
	public static void onGuiScreen(GuiOpenEvent event) {
		try {
			if (event.getGui() instanceof GuiChat) {
				event.setGui(new TextChatGui(((GuiChat) event.getGui()).defaultInputFieldText));
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@SubscribeEvent
	public static void logout(PlayerEvent.PlayerLoggedOutEvent event) {
		PojoSmile.clearAll();
	}

	@SubscribeEvent
	public static void openGui(TickEvent.PlayerTickEvent event) {
		if (event.phase == TickEvent.Phase.START && event.side == Side.CLIENT) {
			if (ClientProxy.key.isPressed()) Minecraft.getMinecraft().displayGuiScreen(new TextChatGui(true));
		}
	}

}
