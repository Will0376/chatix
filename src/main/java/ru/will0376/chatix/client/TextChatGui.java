package ru.will0376.chatix.client;

import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.GuiButton;
import net.minecraft.client.gui.GuiChat;
import net.minecraft.client.gui.ScaledResolution;
import net.minecraft.client.renderer.BufferBuilder;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.client.renderer.RenderHelper;
import net.minecraft.client.renderer.Tessellator;
import net.minecraft.client.renderer.vertex.DefaultVertexFormats;
import net.minecraft.client.resources.I18n;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.text.TextFormatting;
import org.lwjgl.input.Keyboard;
import org.lwjgl.input.Mouse;
import ru.justagod.cutter.GradleSide;
import ru.justagod.cutter.GradleSideOnly;
import ru.will0376.chatix.Chatix;
import ru.will0376.chatix.client.utils.GuiButtonSome;
import ru.will0376.chatix.client.utils.GuiHelper;
import ru.will0376.chatix.client.utils.PojoSmile;
import ru.will0376.chatix.common.PlayerStatistic;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Optional;

import static java.util.Comparator.comparing;

@GradleSideOnly(GradleSide.CLIENT)
public class TextChatGui extends GuiChat {
	private static boolean opened = false;
	private static boolean sortByUsage = false;
	private final int itemsInPage = 42;
	GuiButtonSome buttonOpenGui;
	ArrayList<PojoSmile> values = PojoSmile.getList();
	boolean down = false;
	private int localpage = 1;
	private int maxPages = 1;
	private GuiButton right, left, sort;
	private int xCoord = width - 150;
	private int yCoord = height - 160;
	private int scaledWidth = 0;

	public TextChatGui(boolean open) {
		opened = open;
	}

	public TextChatGui() {
	}

	public TextChatGui(String input) {
		this.defaultInputFieldText = input;
	}

	public void initGui() {
		try {
			buttonList.clear();
			localpage = 1;
			buttonList.add(buttonOpenGui = new GuiButtonSome(-1, "textures/0.png", "null", width - 18, height - 14, 10, 10));
			buttonList.add(sort = new GuiButton(0, width - 20, height - 180, 20, 20, "S"));
			buttonList.add(left = new GuiButton(1, width - 105, height - 34, 18, 20, "<"));
			buttonList.add(right = new GuiButton(2, width - 55, height - 34, 18, 20, ">"));
			super.initGui();
			if (!PojoSmile.getList().isEmpty()) {
				PojoSmile smile = PojoSmile.getRandom();
				buttonOpenGui.setSelectedResource(smile.getResourceLocation());
			} else buttonOpenGui.setSelectedResource(null);// Если нулл - рисуется по первому пути.
			ScaledResolution scaledResolution = new ScaledResolution(mc);
			scaledWidth = scaledResolution.getScaledWidth();
			sorting();
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}

	@Override
	protected void actionPerformed(GuiButton button) throws IOException {
		super.actionPerformed(button);
		switch (button.id) {
			case -1:
				opened = !opened;
				break;
			case 0:
				sortByUsage = !sortByUsage;
				sorting();
				mc.displayGuiScreen(this);
				break;
			case 1:
				if (this.localpage != 1) --this.localpage;
				break;
			case 2:
				if (this.localpage != this.maxPages) ++this.localpage;
				break;
		}
	}

	private void sorting() {
		if (!values.isEmpty()) if (!sortByUsage) values.sort(comparing(PojoSmile::getId));
		else {
			values.sort(comparing(x -> PlayerStatistic.getClientPojo().getSmileMap().getOrDefault(x.getName(), 0)));
			Collections.reverse(values);
		}
	}

	@Override
	public void drawScreen(int mouseX, int mouseY, float partialTicks) {
		try {
			if (opened) {
				left.visible = (localpage != 1);
				right.visible = (localpage != maxPages);
				sort.visible = true;
				drawGradientRect(width - 150, height - 180, this.width, this.height - 14, -1072689136, -804253680);
				drawAll(mouseX, mouseY);
			} else {
				left.visible = false;
				right.visible = false;
				sort.visible = false;
			}
			super.drawScreen(mouseX, mouseY, partialTicks);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void drawAll(int mouseX, int mouseY) throws Exception {
		RenderHelper.enableGUIStandardItemLighting();
		drawCenteredString(super.fontRenderer, I18n.format("gui.page.of", localpage, maxPages), width - 71, height - 29, 16777215);
		drawString(super.fontRenderer, I18n.format(sortByUsage ? "gui.emoji.sort" : "gui.emoji.all"), width - 140, height - 174, 16777215);
		int localX = 0, localY = 0;
		if (!PojoSmile.getUsages().isEmpty()) {
			Optional<PojoSmile> pojoSmileOptional = Optional.empty();

			for (PojoSmile smile : values) {
				int offset = values.indexOf(smile);

				for (int k = 1; k < values.size(); ++k) {
					if (k == 1) {
						this.maxPages = k;
						if (this.localpage == 1 && offset < itemsInPage) {
							moveCoord();
							renderSmile(smile);
							if (mouseX >= this.xCoord && mouseX < this.xCoord + smile.getWidth() + (smile.getWidth() / 2) && mouseY >= this.yCoord && mouseY < this.yCoord + smile
									.getHeight() + (smile.getHeight() / 2)) {
								localX = xCoord;
								localY = yCoord;
								pojoSmileOptional = Optional.of(smile);
							}
						}
					} else {
						if (offset >= itemsInPage * (k - 1) && offset < itemsInPage * k) this.maxPages = k;

						if (this.localpage == k && offset >= itemsInPage * (k - 1) && offset < itemsInPage * k) {
							moveCoord();
							renderSmile(smile);
							if (mouseX >= this.xCoord && mouseX < this.xCoord + smile.getWidth() + (smile.getWidth() / 2) && mouseY >= this.yCoord && mouseY < this.yCoord + smile
									.getHeight() + (smile.getHeight() / 2)) {
								localX = xCoord;
								localY = yCoord;
								pojoSmileOptional = Optional.of(smile);
							}
						}
					}
				}
			}

			int finalLocalX = localX;
			int finalLocalY = localY;

			pojoSmileOptional.ifPresent((smile) -> { // Используется против бага с рендером
				renderSmileBG(finalLocalX + 2, finalLocalY + 2, smile.getWidth() + (smile.getWidth() / 2), smile.getHeight() + (smile
						.getHeight() / 2));

				if (Keyboard.isKeyDown(Keyboard.KEY_LSHIFT)) {
					ArrayList<String> list = new ArrayList<>();
					list.add(TextFormatting.UNDERLINE + smile.getName());
					list.addAll(smile.getUsage());
					drawHoveringText(list, mouseX, mouseY);
				}

				if (Mouse.isButtonDown(0)) {
					if (!down) {
						inputField.setText(inputField.getText() + " " + smile.getUsage().get(0) + " ");
						down = true;
					}
				} else down = false;
			});
			this.yCoord = height - 160;
			this.xCoord = width - 168;
		}
	}

	private void renderSmile(PojoSmile smile) {
		try {
			GuiHelper.bindTexture(smile.getResourceLocation());
			GuiHelper.cleanRender(this.xCoord + 4 - smile.getOffsetX(), this.yCoord + 4 - smile.getOffsetY(), smile.getWidth() + (smile
					.getWidth() / 6), smile.getHeight() + (smile.getHeight() / 6), 1);
		} catch (Exception ex) {
			System.err.println("Error: " + smile.getName());
			System.err.println("Path: " + smile.getPath());
			ex.printStackTrace();
		}
	}

	private void moveCoord() {
		if (this.xCoord >= scaledWidth - 40) {
			this.xCoord = width - 148;
			this.yCoord += 20;
		} else this.xCoord += 20;
	}

	private void renderSmileBG(int xCoord, int yCoord, int w, int h) {
		Tessellator tessellator = Tessellator.getInstance();
		BufferBuilder bufferbuilder = tessellator.getBuffer();
		ResourceLocation skin = new ResourceLocation(Chatix.MOD_ID, "textures/wasted.png");
		Minecraft.getMinecraft().getTextureManager().bindTexture(skin);
		GlStateManager.enableBlend();
		GlStateManager.color(1.0F, 1.0F, 1.0F, Math.min(Math.min(0.5F, 1.0F), 1.0F));
		bufferbuilder.begin(7, DefaultVertexFormats.POSITION_TEX);
		bufferbuilder.pos(xCoord, yCoord + h, 0).tex(0, 1).endVertex();
		bufferbuilder.pos(xCoord + w, yCoord + h, 0).tex(1, 1).endVertex();
		bufferbuilder.pos(xCoord + w, yCoord, 0).tex(1, 0).endVertex();
		bufferbuilder.pos(xCoord, yCoord, 0).tex(0, 0).endVertex();
		tessellator.draw();
		GlStateManager.disableBlend();
	}
}
