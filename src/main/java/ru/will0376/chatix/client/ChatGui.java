package ru.will0376.chatix.client;

import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.ChatLine;
import net.minecraft.client.gui.FontRenderer;
import net.minecraft.client.gui.GuiNewChat;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.util.ReportedException;
import net.minecraft.util.math.MathHelper;
import org.lwjgl.input.Mouse;
import ru.justagod.cutter.GradleSide;
import ru.justagod.cutter.GradleSideOnly;
import ru.will0376.chatix.client.utils.GuiHelper;
import ru.will0376.chatix.client.utils.PojoSmile;
import ru.will0376.chatix.utils.Parse;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

@GradleSideOnly(GradleSide.CLIENT)
public class ChatGui extends GuiNewChat {
	public static boolean disableHover = false;
	public static int limit = 10;

	public ChatGui(Minecraft mcIn) {
		super(mcIn);
	}

	@Override
	public void drawChat(int updateCounter) {
		if (this.mc.gameSettings.chatVisibility != EntityPlayer.EnumChatVisibility.HIDDEN) {
			int i = this.getLineCount();
			int j = this.drawnChatLines.size();
			float f = this.mc.gameSettings.chatOpacity * 0.9F + 0.1F;
			if (j > 0) {
				boolean flag = false;
				if (this.getChatOpen()) {
					flag = true;
				}
				float f1 = this.getChatScale();
				int k = MathHelper.ceil((float) this.getChatWidth() / f1);
				GlStateManager.pushMatrix();
				GlStateManager.translate(2.0F, 8.0F, 0.0F);
				GlStateManager.scale(f1, f1, 1.0F);
				int l = 0;
				int i1;
				int j1;
				int l1;
				List<Render> hoverRenders = new ArrayList<>();
				for (i1 = 0; i1 + this.scrollPos < this.drawnChatLines.size() && i1 < i; ++i1) {
					ChatLine chatline = this.drawnChatLines.get(i1 + this.scrollPos);
					if (chatline != null) {
						j1 = updateCounter - chatline.getUpdatedCounter();
						if (j1 < 200 || flag) {
							double d0 = (double) j1 / 200.0D;
							d0 = 1.0D - d0;
							d0 *= 10.0D;
							d0 = MathHelper.clamp(d0, 0.0D, 1.0D);
							d0 *= d0;
							l1 = (int) (255.0D * d0);
							if (flag) {
								l1 = 255;
							}

							l1 = (int) ((float) l1 * f);
							++l;
							if (l1 > 3) {
								String input = chatline.getChatComponent().getFormattedText();
								String validInput = input.replaceAll("(§.)", "");
								GlStateManager.enableBlend();
								int j2 = -i1 * 16;
								drawRect(-2, j2 - 9, k + 104, j2 + 7, l1 / 2 << 24);
								GlStateManager.enableBlend();
								FontRenderer renderer = this.mc.fontRenderer;
								int mouseX = Mouse.getX() / 2;
								int mouseY = ~Mouse.getY() / 2 + 43;
								List<Render> smileRenders = new ArrayList<>();

								if (input.contains("!clear!")) {
									clearChatMessages(false);
								}
								for (String usage : Parse.parseSmilesUsages(validInput)) {
									while (input.contains(usage)) {
										PojoSmile smile = PojoSmile.getUsages().get(usage);
										int width = smile.getWidth();
										int height = smile.getHeight();
										double x = renderer.getStringWidth(validInput.substring(0,
												validInput.indexOf(usage) + 1)) + (double) width / 2 + 5;
										double y = j2 - (2 + (double) height / 2) + 1;
										smileRenders.add(() -> {
											try {
												mc.getTextureManager().bindTexture(smile.getResourceLocation());
												GuiHelper.cleanRender(x - 12, y, width, height, 1);
											} catch (ReportedException exception) {
												exception.printStackTrace();
											}
										});
										hoverRenders.add(() -> {
											if (mouseX >= x - 12
													&& mouseX <= x - 12 + width * 1.3
													&& mouseY >= y
													&& mouseY < y + height * 1.3
													&& getChatOpen()) {
												drawHoveringTexts(usage, mouseX, mouseY, mc.fontRenderer);
												if (Mouse.isButtonDown(0)) {
													mc.player.sendChatMessage(usage);
													mc.displayGuiScreen(null);
												}
											}
										});
										input = input.replaceFirst(usage, emptyStringByWidth(width / 2));
										validInput = input.replaceAll("(§.)", "");
									}
								}
								GuiHelper.drawScalledString(renderer, 0, (j2 - 4), 1f, 1f, 1, input, -1);
								smileRenders.forEach(Render::onRender);
								GlStateManager.disableAlpha();
								GlStateManager.disableBlend();
							}
						}
					}
				}
				if (!disableHover)
					hoverRenders.forEach(Render::onRender);
				if (flag) {
					i1 = this.mc.fontRenderer.FONT_HEIGHT;
					GlStateManager.translate(-3.0F, 0.0F, 0.0F);
					int l2 = j * i1 + j;
					j1 = l * i1 + l;
					int j3 = this.scrollPos * j1 / j;
					int k1 = j1 * j1 / l2;
					if (l2 != j1) {
						l1 = j3 > 0 ? 170 : 96;
						int l3 = this.isScrolled ? 13382451 : 3355562;
						drawRect(0, -j3, 2, -j3 - k1, l3 + (l1 << 24));
						drawRect(2, -j3, 1, -j3 - k1, 13421772 + (l1 << 24));
					}
				}
				GlStateManager.popMatrix();
			}
		}

	}

	private String emptyStringByWidth(int width) {
		String string = "";
		while (this.mc.fontRenderer.getStringWidth(string) < width) {
			string += " ";
		}
		string += "   ";
		return string;
	}

	protected void drawHoveringTexts(String hoverText, double xCoord, double yCoord, FontRenderer font) {
		List<String> list = Collections.singletonList(hoverText);
		GlStateManager.disableRescaleNormal();
		GlStateManager.disableDepth();
		int k = 0;
		Iterator<String> iterator = list.iterator();
		int k2;
		while (iterator.hasNext()) {
			String j2 = iterator.next();
			k2 = font.getStringWidth(j2);
			if (k2 > k)
				k = k2;
		}
		int var15 = (int) (xCoord + 12);
		k2 = (int) (yCoord - 12);
		int i1 = 8;
		zLevel = 300;
		int j1 = -267386864;
		this.drawGradientRect(var15 - 3, k2 - 4, var15 + k + 3, k2 - 3, j1, j1);
		this.drawGradientRect(var15 - 3, k2 + i1 + 3, var15 + k + 3, k2 + i1 + 4, j1, j1);
		this.drawGradientRect(var15 - 3, k2 - 3, var15 + k + 3, k2 + i1 + 3, j1, j1);
		this.drawGradientRect(var15 - 4, k2 - 3, var15 - 3, k2 + i1 + 3, j1, j1);
		this.drawGradientRect(var15 + k + 3, k2 - 3, var15 + k + 4, k2 + i1 + 3, j1, j1);
		int k1 = 1347420415;
		int l1 = (k1 & 16711422) >> 1 | k1 & -16777216;
		this.drawGradientRect(var15 - 3, k2 - 3 + 1, var15 - 3 + 1, k2 + i1 + 3 - 1, k1, l1);
		this.drawGradientRect(var15 + k + 2, k2 - 3 + 1, var15 + k + 3, k2 + i1 + 3 - 1, k1, l1);
		this.drawGradientRect(var15 - 3, k2 - 3, var15 + k + 3, k2 - 3 + 1, k1, k1);
		this.drawGradientRect(var15 - 3, k2 + i1 + 2, var15 + k + 3, k2 + i1 + 3, l1, l1);
		for (String s1 : list) {
			font.drawStringWithShadow(s1, var15, k2, -1);
			k2 += 10;
		}
		zLevel = 0;
		GlStateManager.enableDepth();
		GlStateManager.enableRescaleNormal();
	}

	public interface Render {
		void onRender();
	}
}
