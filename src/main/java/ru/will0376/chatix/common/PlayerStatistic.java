package ru.will0376.chatix.common;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;
import com.google.gson.stream.JsonReader;
import ru.justagod.cutter.GradleSide;
import ru.justagod.cutter.GradleSideOnly;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.Map;

public class PlayerStatistic {
	private static final Map<String, PlayerPojo> playerMap = new HashMap<>();
	@GradleSideOnly(GradleSide.CLIENT)
	private static PlayerPojo clientPojo;
	@GradleSideOnly(GradleSide.SERVER)
	private static File playersDir;

	@GradleSideOnly(GradleSide.SERVER)
	public static void loadPlayersStatistics() throws IOException {
		Files.walk(Paths.get(playersDir.getAbsolutePath()))
				.filter(Files::isRegularFile)
				.forEach(js -> {
					try {
						if (js != null && js.toFile().getName().contains(".json")) {
							String nick = js.toFile().getName().replace(".json", "");
							PlayerPojo tmpPojo = new GsonBuilder().create().fromJson(new JsonReader(new FileReader(js.toString())), PlayerPojo.class);
							if (tmpPojo == null || tmpPojo.getSmileMap() == null)
								tmpPojo = new PlayerPojo().createNewPojo();
							playerMap.put(nick, tmpPojo);
						}
					} catch (FileNotFoundException e) {
						e.printStackTrace();
					}
				});
	}

	@GradleSideOnly(GradleSide.SERVER)
	public static Map<String, PlayerPojo> getPlayerMap() {
		return playerMap;
	}

	@GradleSideOnly(GradleSide.SERVER)
	public static PlayerPojo safeGetProfile(String nick) {
		if (!playerMap.containsKey(nick)) {
			PlayerPojo ret = new PlayerPojo();
			ret.setSmileMap(new HashMap<>());
			playerMap.put(nick, ret);
			return ret;
		}
		return playerMap.get(nick);
	}

	@GradleSideOnly(GradleSide.SERVER)
	public static void safeSaveAll() {
		playerMap.forEach((s, pojo) -> {
			try (FileWriter writer = new FileWriter(new File(playersDir, s + ".json"))) {
				Gson gson = new GsonBuilder().setPrettyPrinting().create();
				gson.toJson(pojo.getJson(), writer);
			} catch (IOException e) {
				e.printStackTrace();
			}
		});
	}

	@GradleSideOnly(GradleSide.CLIENT)
	public static void clientIncrement(String smileName) {
		clientPojo.increment(smileName);
	}

	@GradleSideOnly(GradleSide.SERVER)
	public static void serverIncrement(String nick, String smileName) {
		safeGetProfile(nick).increment(smileName);
	}

	@GradleSideOnly(GradleSide.CLIENT)
	public static PlayerPojo getClientPojo() {
		return clientPojo;
	}

	@GradleSideOnly(GradleSide.CLIENT)
	public static void setClientPojo(PlayerPojo pojo) {
		PlayerStatistic.clientPojo = pojo;
	}

	@GradleSideOnly(GradleSide.CLIENT)
	public static void setClientPojoByJson(JsonObject jsonObject) {
		clientPojo = new GsonBuilder().create().fromJson(jsonObject, PlayerPojo.class);
	}

	@GradleSideOnly(GradleSide.SERVER)
	public static void setPlayersDir(File dir) {
		playersDir = new File(dir.getAbsolutePath(), "ChatiXPlayers");
		playersDir.mkdir();
	}
}