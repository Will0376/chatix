package ru.will0376.chatix.common;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import java.util.HashMap;
import java.util.Map;

public class PlayerPojo {
	private Map<String, Integer> smileMap;

	public Map<String, Integer> getSmileMap() {
		return smileMap;
	}

	public void setSmileMap(Map<String, Integer> smileMap) {
		this.smileMap = smileMap;
	}

	public void increment(String smileName) {
		int counter = smileMap.getOrDefault(smileName, 0);
		smileMap.put(smileName, ++counter);
	}

	public PlayerPojo createNewPojo() {
		PlayerPojo tmp = new PlayerPojo();
		tmp.setSmileMap(new HashMap<>());
		return tmp;
	}

	public JsonObject getJson() {
		Gson gson = new GsonBuilder().setPrettyPrinting().create();
		return (JsonObject) new JsonParser().parse(gson.toJson(this));
	}
}
