package ru.will0376.chatix.common.net;

import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import io.netty.buffer.ByteBuf;
import net.minecraft.client.Minecraft;
import net.minecraft.util.text.TextComponentString;
import net.minecraftforge.fml.common.network.ByteBufUtils;
import net.minecraftforge.fml.common.network.simpleimpl.IMessage;
import net.minecraftforge.fml.common.network.simpleimpl.IMessageHandler;
import net.minecraftforge.fml.common.network.simpleimpl.MessageContext;
import ru.justagod.cutter.invoke.Invoke;
import ru.will0376.chatix.client.ChatGui;
import ru.will0376.chatix.client.utils.GenerateJson;
import ru.will0376.chatix.client.utils.PojoSmile;
import ru.will0376.chatix.common.PlayerStatistic;

public class ToClient implements IMessage {
	/*
	 * -1 - Приём статистики
	 * -2 - Инкремент по названию
	 * 1 - Генерация json для файлов.
	 * 2 - Очистить список
	 * 3 - Добавить в список
	 * 4 - Ре-скан папок в ChatiX.
	 * */
	int mode;
	String text;
	boolean disableHover;
	int limit;

	public ToClient() {
	}

	public ToClient(int mode, String text, boolean disableHover, int limit) {
		this.mode = mode;
		this.text = text;
		this.disableHover = disableHover;
		this.limit = limit;
	}

	public ToClient(int mode) {
		this.mode = mode;
		text = "";
		disableHover = false;
		limit = 1;
	}

	@Override
	public void fromBytes(ByteBuf buf) {
		text = ByteBufUtils.readUTF8String(buf);
		mode = buf.readInt();
		limit = buf.readInt();
		disableHover = buf.readBoolean();
	}

	@Override
	public void toBytes(ByteBuf buf) {
		ByteBufUtils.writeUTF8String(buf, text);
		buf.writeInt(mode);
		buf.writeInt(limit);
		buf.writeBoolean(disableHover);
	}

	public static class Handler implements IMessageHandler<ToClient, IMessage> {
		@Override
		public IMessage onMessage(ToClient message, MessageContext ctx) {
			Invoke.client(() -> {
				ChatGui.disableHover = message.disableHover;
				ChatGui.limit = message.limit;
				switch (message.mode) {
					case -1:
						PlayerStatistic.setClientPojoByJson((JsonObject) new JsonParser().parse(message.text));
						break;

					case -2:
						PlayerStatistic.clientIncrement(message.text);
						break;

					case 1:
						GenerateJson.generateFiles();
						Minecraft.getMinecraft().player.sendMessage(new TextComponentString("[GenerateJson] => Done!"));
						break;

					case 2:
						Minecraft.getMinecraft().ingameGUI.getChatGUI().clearChatMessages(false);
						Minecraft.getMinecraft().displayGuiScreen(null);
						PojoSmile.clearAll();
						break;

					case 3:
						PojoSmile.createFromJson((JsonObject) new JsonParser().parse(message.text));
						break;

					case 4:
						GenerateJson.smileFiles.clear();
						GenerateJson.deepScanFiles();
						break;
				}
			});
			return null;
		}
	}
}
